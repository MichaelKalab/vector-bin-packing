**Vector Bin Packing**

This command line tool analyzes instances for Vector Bin Packing given by an input file, using the algorithms and bounding procedures presented in the paper 'Heuristics for Vector Bin Packing' by Rina Panigraphy et al. For explanations of those heuristics, see
https://www.microsoft.com/en-us/research/wp-content/uploads/2011/01/VBPackingESA11.pdf

The command line tool asks the user for the path and the name of the input file. It also asks for the path, the prefix and the suffix of the output files, which algorithms and/or bounding procedures should be executed and which output should be written to a file. More information about the final names of the output files is given in 'Output'. Specifications about this handling are explained in the command line. We thus omit a detailed description here.


**Requirements**

If you have not been provided with the executable you need to compile the package. Further information on how to compile is found in ‚Compilation and Execution‘.

The required libraries are all contained in version C++11 ore later, they are

<iostream>
<vector>
<fstream>
<stiod.h>
<string>
<numeric>
<math.h>
<cstdlib>


**Compilation and Execution**

The compilation uses a makefile contained in the package folder.

If you run OSX or Linux operating system open your command line (‚Console‘, ‚Terminal‘) and go to the folder where the files of VBPacking are stored.
Enter either ‚make clean‘ to compile and remove .o-files or ‚make‘ just to compile.
Compilation should start.
To execute in OSX go to the folder again and click (or double click according to your settings) on vbpacking. The program should start automatically in your command line.

To execute in Linux go to the folder and enter the command ‚chmod +x vbpacking‘‚ then the program is available for execution.
To execute enter the command ‚./vbpacking.exe‘‚ the program should start automatically in your command line.

If you run Windows and you do not possess a GUI for C++ you should probably consider downloading Cygwin at cygwin.com.
Download the correct version (32 or 64 bit). During installation make sure that the required packages are installed. These may be found at 
https://cs.calvin.edu/courses/cs/112/resources/installingEclipse/cygwin/

To execute open Cygwin Terminal and enter the command ‚cd /cygwin/c‘. Now go to your program folder and enter the command ‚./vbpacking.exe‘. The program should start now. 

**Input Specifications**

An example for an input file can be found in the program package, called ‚example_input‘.
The program accepts text files of the following syntax:

<dim> 
<cap>
<val1>
<val2> 
<val3> 
<val4>
<val5> 
<val6>
… 


Example:
2
100.5
34.5
22.345
12.345
67.435
34.3434
…

The delimiter between each line may (instead of a newline) as well a whitespace.
'dim' accepts any number bigger than one and specifies the dimension of an instance. If dim is not integer it is rounded closer to 0. The following positions are all stored as double types require a ‚.‘ for a comma.

‚cap' accepts any non-negative number and specifies the capacity of each bin where it is assumed that the capacities are equal in each dimension.
The following data val1, val2, … are the demands of the items listed first by item and then by dimension, i.e. if dim = 3 then the values val1, val2 and val3 are assigned to the demands of the first item, val4, val5 and val6 are assigned to the second and so on. 
If the number of values is not divisible by 'dim' the list is truncated so that a maximum number of items is generated and for each item there are enough values available. The following error messages are printed if the input file fails to fulfill the above specifications.
	
If at most one position is given the process exits and prints 'Input file has too few arguments. Process aborted!'
	 
If the value in position 'dim' is smaller than 1 the process exits and prints 'Dimension must be positive! Process aborted!'

If any value in any position is not numeric it is set to $0$ without any further mentioning.
	 
If the value in position 'cap' is non-positive the program exits and prints 'Capacity must be non-negative. Process aborted!'
	 
If any of the values in position val1, val2, … is bigger than cap the program exits displaying the error message Value in position <pos> is bigger than the capacity. Process aborted!' where <pos> is the position of the value.
	 
If any of the values val1, val2, … is negative the programm prints 'Value in position <pos> is negative. Process aborted!' where <pos> is the position of the value where we start counting at <dim>.


**Input and Output Handling**

Input and Ouput Handling are explained by the command line. We only give a brief description here.

The program asks for the location-and-name of the input file. It further asks where the output file should be stored and for a dummy name for the output file without the suffix (e.g. txt). Since more then one output file may be generated all output files containing solutions receive the suffix of the corresponding algorithm name. The output file containing the bounds receives the suffix ‚bounds‘.
It further asks for a suffix for the output files, e.g. ‚txt‘ or ‚csv‘. 

The command line asks then for the algorithms and bounds that should be executed. There are two options available: Either enter the command ‚A‘ and all algorithms and bounds are executed and the solutions and bounds stored to files. Alternatively it accepts a string containing Y’s and N’s for yes and no. Each of the Y’s and N’s stands for an algorithm/bound of the following order

RandomFit wFFDSum FFDSum BF* BF BFDot* BFDot BFLINF* BFLINF LC L2 LH.

After this the algorithms are executed and the output is written to files contained in the path entered by the handling explained above.

**Output Specifications**

Syntax of the output files containing the solution and the number of bins needed:
 
 number_of_bins, <number_of_bins> 
 <item>, <bin> 
 <item>, <bin> 
 <item>, <bin> 
 <item>, <bin> 
 <item>, <bin> 
 … 
 
<item> is the index of the item where the items are ordered according to their appearance in the input file, <bin> is the index of the bin to which item <item> is assigned.

Syntax of the output file containing the bounds:
 
LC, <LC>
L2, <L2>
LH, <LH>

